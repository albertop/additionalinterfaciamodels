/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | Unsupported Contributions for OpenFOAM
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2014 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
2014-02-19 Alberto Passalacqua: Implemented Tenneti et al. drag correlation.
-------------------------------------------------------------------------------
License
    This file is a derivative work of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "Tenneti.H"
#include "phasePair.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace dragModels
{
    defineTypeNameAndDebug(Tenneti, 0);
    addToRunTimeSelectionTable(dragModel, Tenneti, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::dragModels::Tenneti::Tenneti
(
    const dictionary& dict,
    const phasePair& pair,
    const bool registerObject
)
:
    dragModel(dict, pair, registerObject),
    residualRe_("residualRe", dimless, dict.lookup("residualRe"))
{}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::dragModels::Tenneti::~Tenneti()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

Foam::tmp<Foam::volScalarField> Foam::dragModels::Tenneti::CdRe() const
{
    volScalarField alpha1(max(pair_.dispersed(), residualAlpha_));
    volScalarField alpha2(max(scalar(1) - pair_.dispersed(), residualAlpha_));

    volScalarField Res(alpha2*pair_.Re());
    volScalarField ReLim
    (
        "ReLim",
        max(Re, residualRe_)
    );

    volScalarField CdReIsolated
    (
        neg(Res - 1000)*24.0*(1.0 + 0.15*pow(Res, 0.687))
      + pos(Res - 1000)*0.44*max(Res, residualRe_)
    );

    volScalarField F0
    (
        5.81*alpha1/pow(alpha2, 3) + 0.48*pow(alpha1, 1.0/3.0)/pow(alpha2, 4)
    );

    volScalarField F1
    (
        pow(alpha1, 3)*ReLim*(0.95 + 0.61*pow(alpha1, 3)/sqr(alpha2))
    );

    // AP: Tenneti et al. correlation includes the mean pressure drag.
    //     This has been removed here by multiplying F by alpha2 for
    //     consistency with the formulation used in OpenFOAM
    //
    //     Also, we have
    //
    //     Kliterature = 18*alpha1*alpha2*mu_2*F/d^2
    //
    //     KOpenFOAM = 3/4*Cd*rhog/(alpha2*d^2) = Kliterature/(alpha1*alpha2)
    //
    //     So here we return 24*F*alpha2

    return CdReIsolated + 24.0*sqr(alpha2)*(F0 + F1);
}


// ************************************************************************* //
